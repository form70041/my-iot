# IoT project

## Hardware
- ESP32
- DHT22
- SSD1306

## Software
- MQTT protocol
- Broker Mosquitto
- SQLite database

## References

- https://mqtt.org/
- https://mosquitto.org/
- https://pypi.org/project/paho-mqtt/
https://pypi.org/project/dataset/

## Objectives
Send realtime temperature and humidity to clients in Python.

## Python Code

### Virtual environment

__create__

    python -m venv venv

__delete__

delete folder `venv`

__append packages to `requirements.txt`__
    
    pip freeze > requirements.txt

## Local operations

__build__

    docker build -t gpessoa2023/iot .

__run__

    docker run -d --name my-iot -p 80:5000 --link redis gpessoa2023/iot

__dependency__
    docker run -d --name redis -p 6379:6379 redis:alpine