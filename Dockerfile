FROM python:3.11-alpine

EXPOSE 5000
WORKDIR /src

# Copia o requirements para src/
COPY requirements.txt .

# Instala o requirements.txt
RUN pip install -r requirements.txt

# Copia tudo
COPY . .

# Executa o módulo
CMD ["python", "-m", "iot"]

# Poderia ser feito 
# CMD "python -m iot"
# Mas isso poderia fazer com que o Docker confundisse
# o -m com algum comando interno do Docker (caso, ao invés de -m fosse -p, por exemplo)
# No caso do Django, para as migrations, pode ser executar comando utilizando o CMD,
# mas é recomendado criar yum ficheiro ENTRYPOINT (pesqusiar)