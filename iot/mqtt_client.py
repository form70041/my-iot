import datetime as dt

import paho.mqtt.client as mqtt
import dataset
import redis

try:
    from iot import settings
except ModuleNotFoundError:
    import settings

# redis connection
cache = redis.Redis(host=settings.REDIS_URL, port=6379)

def on_connect_callback(client, userdata, flags, rc):
    if (rc == 0):
        print('Connection OK')
        topic = 'esp32/+/sensors/#'
        client.subscribe(topic)
        client.message_callback_add(topic, on_message_callback)
    else:
        print('Connection Failed')


def on_message_callback(client, userdata, msg):
    topic = msg.topic.split('/')[-1]
    payload = float(msg.payload)

    now = dt.datetime.now()

    data = {'timestamp': now, 'value': payload}
    db = dataset.connect(settings.DATABASE)

    # update redis
    cache.set(topic, str(data))

    #print(dt.datetime.now(), topic, float(msg.payload), sep=' : ')
    print(f'{now} : {topic:>12} : {payload:.2f}')
    table = db[topic]
    table.insert({'timestamp': now, 'value': payload})
    db.close()

def main():
    Client = mqtt.Client(client_id='gabriel-laptop')
    Client.on_connect = on_connect_callback  # pointer to function
    Client.username_pw_set('cfreire', '65Zc2E')
    Client.connect('iot.cfreire.pt')
    Client.loop_forever()


if __name__ == '__main__':
    main()
