import logging

from flask import Flask, render_template, jsonify, request
from waitress import serve
import dataset
import redis
import datetime

try:
    from iot import settings
except ModuleNotFoundError:
    import settings

app = Flask(__name__)
logger = logging.getLogger(__name__)
cache = redis.Redis(host=settings.REDIS_URL, port=6379)

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/realtime')
def realtime():
    logger.info('Realtime function call')
    data = eval(cache.get('temperature'))
    logger.debug(data)
    return jsonify(data)

@app.route('/history')
def history():
    try:
        limit = int(request.args.get('limit'))
    except (TypeError, ValueError):
        limit = 10
    db = dataset.connect(settings.DATABASE)
    result = db.query(f'SELECT timestamp, value FROM temperature '
                      f'ORDER BY timestamp DESC LIMIT {limit}')

    data = [row for row in result]
    db.close()
    logger.debug(data)

    return jsonify(data)

def main():
    logger.info(f'Starting IoT version {settings.__version__}')
    serve(app, host='0.0.0.0', port=5000, threads=5)
    
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()