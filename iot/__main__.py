import threading

try:
    from iot import mqtt_client
    from iot import rest_api
except ModuleNotFoundError:
    import mqtt_client
    import rest_api


if __name__ == '__main__':
    threading.Thread(target=mqtt_client.main).start()
    threading.Thread(target=rest_api.main).start()