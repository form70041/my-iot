import sys
import logging

from iot import settings


if (sys.version_info[:2] < settings.MIN_PYTHON_VERSION):
    print(
        'Error: minimum Python version required:',
        settings.MIN_PYTHON_VERSION)
    sys.exit(1)

logging.basicConfig(filename='my_sum.log', level=logging.INFO)

logger = logging.getLogger(__name__)
logger.info(f'Starting {settings.APP_NAME} version v{settings.__version__}')
logger.setLevel(settings.DEFAULT_LOG_LEVEL)
