__version__ = '1.1.1'

MIN_PYTHON_VERSION = (3, 8)
APP_NAME = 'iot'

DEFAULT_LOG_LEVEL = 'INFO'

DATABASE = 'sqlite:///db/sensors-data.sqlite'

REDIS_URL = 'redis'